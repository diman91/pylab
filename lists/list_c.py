# list example iii
#

def main():
    # A string of a few teams
    my_teams = "Olympiakos PAOK AEK Panathinaikos Aris OFI"
    # Split above teams string and save to teams
    teams = my_teams.split(' ')
    # A list of a few more teams
    more_teams = ["AEL", "Atromitos", "Panetolikos", "Asteras"]
    # Add more_teams teams into teams list
    i = 0
    teams_to_append = len(more_teams)
    while i<teams_to_append:
        teams.append(more_teams.pop())
        i += 1
        
    i = 1
    for team in teams:
        print("Team %d: %s\n" %(i, team))
        i += 1
    
    
if __name__ == "__main__":
    main()
    