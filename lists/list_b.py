import sys

def print_the_list(my_list):
    """ Print every list item in one line using stdout """
    for item in range(0, len(my_list)):
        sys.stdout.write(str(my_list[item]) + " ")
    print("\nThe list length is: %s " % len(my_list))

def main():
    ints = []
    for i in range(0, 10):
        ints.append(i+1)
    # ints = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # Replace 5 with 55
    five_index = ints.index(5)
    ints[five_index] = 55
    # Insert -1 in the beginning of the list
    # list.insert(index, value)
    ints.insert(0, -1)
    # Append 11 in the end of the list
    ints.append(11)
    print_the_list(ints)
    print()
    # Delete the first value : -1 and the last value : 11
    del ints[-1]            # del the last value, i.e. 11
    ints.remove(-1)         # remove the -1 value since it appears only one time
    ints[five_index] = 5    # restore the 5 value in the index 4 of ints list
    print_the_list(ints)
    # Reverse the ints list
    print("\nThe reversed ints list is: ")
    rev = reversed(ints)
    for item in rev:
        sys.stdout.write(str(item) + " ")

if __name__ == "__main__":
    main()

