import sys

class ListTests:
    """
        A simple class for lists functions exploration
    """
    ints_list = []

    def push(self, num):
        self.ints_list.append(num)

    def printList(self):
        #idx = 0
        for num in range(0, len(self.ints_list)):
            #print("Element %d = %d " % (idx, self.ints_list[num]))
            #idx = idx + 1
            sys.stdout.write(str(self.ints_list[num]) + " ")

    def popAll(self):
        print("\n")
        while(len(self.ints_list)!=0):
            print("Poping element %d from the list ... " % self.ints_list[-1])
            self.ints_list.pop()


if __name__ == "__main__":
    Obj = ListTests()
    for num in range(10, 22, 2):
        Obj.push(num)

    Obj.printList()
    Obj.popAll()
    Obj.printList()

""" OUTPUT
10 12 14 16 18 20 

Poping element 20 from the list ... 
Poping element 18 from the list ... 
Poping element 16 from the list ... 
Poping element 14 from the list ... 
Poping element 12 from the list ... 
Poping element 10 from the list ... 
"""