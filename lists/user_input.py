# Illustrating the input function in python
# Also checking the try/except exceptions mechanism in python
# Last checking the index built-in function for getting the index
# of an item inside a list

def main():
    """ Initialize an empty list of historic greek teams """
    teams = []
    # Enter three teams
    teams.append("Olympiakos")
    teams.append("Panathinaikos")
    teams.append("AEK")
    # Print the teams
    print("Greek historical teams: ")
    print(teams)
    # Change a team that is deprecated
    old_team = input("Which team should I remove? ")
    # search if the team exists in the list
    try:
        # The teams index in the list of teams
        team_index = teams.index(old_team)
        # Get a new team from the user as input
        new_team = input("Please enter the new team name: ")
        # Enter the new team name with the old team name
        teams[team_index] = new_team
        # Print the updated teams list
        print("Here is the updated Greek historical teams: ")
        for tm in teams:
            print(tm)
    except:
        print("The team %s does not exists in the Greek historical teams list! Please try again!" % old_team)

if __name__ == "__main__":
    main()