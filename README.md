# README #

Python simple Examples Repository.

### What is this repository for? ###

* This is a repo with examples from myself & a few nice Internet resources regarding various topics in Python. 
* Version v1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contents ###

* Variables, Strings, Slicing, (Bitwise) Operators, Loops, Conditionals
* Functions (& magic Functions in classes)
* Lists
* Dictionaries
* Regular Expressions
* Classes
* Generators
* Decorators
