# Special Pythagorean triplet
import math
# import sys


def is_pythagorean(a, b, c):
    if math.pow(a, 2) + math.pow(b, 2) == math.pow(c, 2):
        # print("Pythagorean Triplet(" + str(a) + ", " + str(b) + ", " + ", " + str(c) + ")\n")
        return True
    return False


def main():
    N = 5001
    for i in range(1, N):
        for j in range(i+1, N):
            for k in range(j+1, N):
                if is_pythagorean(i, j, k):
                    if i + j + k == 1000:
                        print("Found special Pythagorean Triplet for a + b + c = 1000 --> (" + str(i) + ", " + str(j) + ", " + ", " + str(k) + ")\n")
                        print("Product a*b*c = " + str(a*b*c))
                        # sys.exit(0)
                        break


if __name__ == "__main__":
    main()
