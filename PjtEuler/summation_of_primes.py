# Summation of primes
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.


def is_prime(num):
    for i in range(2, num):
        if num%i == 0:
            return False
    print("Prime Number ==> " + str(num))
    return True


def main():
    sum_of_primes = 0
    N = 11
    for i in range(2, N):
        if is_prime(i):
            sum_of_primes = sum_of_primes + i
    print("Prime Numbers Summation below " + str(N-1) + " = " + str(sum_of_primes))

if __name__ == "__main__":
    main()
