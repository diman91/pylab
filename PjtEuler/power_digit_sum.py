# Power DIgit Sum
# 215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
# What is the sum of the digits of the number 2^1000?
import math

def break_to_digits(num):
    '''
        Returns a list of the number's digits
    '''
    num_digits = []
    while num != 0:
        rest, last = divmod(num, 10)
        num_digits.append(last)
        num = rest
    # print(num_digits[::-1])
    return num_digits

def main():
    '''
        main
    '''
    N = 15
    # 2^15 = 32768
    #
    lst = []
    lst = break_to_digits(math.pow(2, N))
    summation_of_digits = 0
    for i in lst:
        summation_of_digits += i
    print("Summation of " + str(math.pow(2, N)) + " << " + str(lst) + " >> digits is ==> " + str(summation_of_digits))

if __name__ == "__main__":
    '''
        Call main
    '''
    main()
