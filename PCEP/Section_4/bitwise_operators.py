#
# Bitwise Operators in Python
#
# &, |, ^, ~, >>, <<
#

print(2 & 3)    # 2 and 3 = 2

print(2 | 3)    # 2 or 3 = 3

print(2 ^ 3)    # 2 xor 3 = 1

print(~ 3)    # negation

print(10 >> 1)  # same as division by 2 : 10 / 2 = 5

print(10 << 1)  # same as multiplying by 2 : 10 * 2 = 20



#
# div and mult by 4 via bitwise shifting ::
#
print (10 >> 2)

print(10 << 2)

