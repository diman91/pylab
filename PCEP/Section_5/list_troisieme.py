#
# Iterate a list
#

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

numbers_same = []
[numbers_same.append(i) for i in range(11)]

sum = 0


for i in numbers:
    sum += i

print("Sum = " + str(sum))



sum = 0
for i in numbers_same:
    sum += i

print("Same Sum = ", str(sum))



# 
# enumerate function in lists
# 

langs = ["C++", "Python", "Java", "Golang", "Rust"]

for i, j in enumerate(langs):
    print(i, " --> ", j)

