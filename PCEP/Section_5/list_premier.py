#
# Lists in Python simple example
#

teams = ["Olympiacos", "Panathinaikos", "PAOK", "ARIS", "AEK"]

for team in teams:
    if team != "Olympiacos":
        team = "Olympiacos"
        print(team)

print(teams)

#
# Slicing a list in Python
#
print(teams[-1:])
print(teams[-3:-2])
print(teams[-2:-1])
print(teams[-4:-3])

#
# Way 1 to replace item in a list in Python
#
for i in range(len(teams)):
    if teams[i] != "Olympiacos":
        teams[i] = "Olympiacos"

print(teams[:])

for team in teams:
    if team == "Olympiacos":
        teams.remove(team)

print(teams[:])
print("Length of teams = " + str(len(teams)))

# ------------------------------------------------------------ #

teams.clear()
# OR >>
# del teams[:]
# I like clear better btw for all of the list
#
print(teams[:])
print("Length of teams = " + str(len(teams)))
