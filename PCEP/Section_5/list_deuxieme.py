#
# Lists: append() & insert() functions
#

towns = ["Paris", "Lyon", "Grenoble", "Toulouse", "Nice"]

towns.append("Bordeaux")

towns.insert(1, "Marseille")

print(towns)

towns.append("Athens")

print(towns)

#
# remove lastly appended wrong town from the list
#
towns = towns[:-1]

print(towns)


#
# print the list in reversed order
#

print(towns[::-1])



#
# Sorting a list of numbers
#

numbers = [1100, 11, 33, 30, 25, 7]

numbers.sort()

print(numbers)

