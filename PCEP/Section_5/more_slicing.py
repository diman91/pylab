#
# More slicing in lists in Python
#

# [start:end]
# [:2] from start to
# [2:] from 2 to end


cities = ["Paris", "Marseille", "Lyon", "Grenoble", "Nice"]

print(cities[::])

#
# Beginning up to 2nd element
#
print(cities[:2])

#
# From 3rd element up to the end
#
print(cities[3:])


#
# Reverse order
#

print(cities[::-1])



#
# With a step of 2
#
print(cities[::2])


#
# Print last city
#
print(cities[-1])



#
# Search for a town in the list
#

if "Athens" not in cities:
    print("Nope!\n")
else:
    print("Is it a French town for real ?!!!\n")

