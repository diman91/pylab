#
# PCEP Entry Level Python Exam Preparation Lab 
#

-----

- Date: June 6th, 2021

- Topics:

  > Print function

  > Operators

  > Variables

  > Lists

  > Iterations (Loops)

  > Dictionaries (maps)

  > Tuples

  > Functions

  > Classes
