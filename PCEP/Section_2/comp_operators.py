#
# Comparison Operators simple examples
#

print("Dimos" == "dimos")
print("dimos" == "dimos")

print(2.0 == 2)
print(3.0 > 3)

print(True == False)
print(2 != 3)

print((10 % 2) == 0)
print((11 % 2) != 0)

print("Dimosthenis" > "Dimos")
print("Dimosthenis" > "dimosthenis")
print("Dimosthenis" == "Dimos")
print("Dimosthenis" == "dimosthenis")
