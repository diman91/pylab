#
# Your 1st Python program 
#


#
# premier
#
print("Hello World from Python ...", end="!!!\n\n")

print("Hello from \"Python\" ! ")

#
# deuxieme
#
words = ["Hello", "my", "name", "is", "dimos"]

for word in words:
    print(word, sep="\t")


#
# troisieme
#

teams = ["OSFP", "PAO", "AEK", "PAOK", "ARIS"]

for team in teams:
    print(team, end="")

print("\n", end="############################\n")

#
# NOTE: From Linux run as python3 hello_python.py
#
