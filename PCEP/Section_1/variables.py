#
# Variables in Python (integer, float, string, list, dictionary)
#


#
# Numbers
#
my_int = 8
my_float = 8.8

print("Integer = ", my_int)

print("Float = ", my_float)


#
# Strings & list of strings
#
my_first_name = "Dimosthenis"
my_last_name = str("Katsimardos")
my_name_as_list = [my_first_name, my_last_name]

for name in my_name_as_list:
    print(name, sep=" ")


#
# numbers and a map/dictionary of numbers with keys-values paris
#
print("")

lucky_numbers = {
    "Four -->": 8, 
    "Five -->": 10, 
    "Ten -->": 20, 
    "Thirty Eight -->": 77, 
    "Thirty Nine -->": 78, 
    "Fourty -->": 80
}

for key, number in lucky_numbers.items():
    print(key, number/2)


