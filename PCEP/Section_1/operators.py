#
# Operators in Python
#

#
# Binary operators
#
print(2. * 5.) # 10.0

print(2 * 5)  # 10

print(2 ** 5)  # 32


print (11 / 2) # 5.5
print(11 // 2) # 5

print(11 % 2) # 1

print (10 % 2) # 0


print (12 - 5 * 2 / 3 * 10 + 5)

print(10 / 2)
print(int(10/2))


###

name = "George"
last_name = "Clinton"

print(name * 2, last_name *2, sep=" <--> ")

print("haha" * 3)

