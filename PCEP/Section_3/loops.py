#
# While Loops
#

correct_number = 28

iterations = 0

while True:
    your_guess = int(input("Please guess the number: "))

    if your_guess == correct_number:
        print("Congrats, you found my magic number!\n")
        break
    elif your_guess > correct_number:
        print("You have chosen a bigger number. Guess a smaller one please ... \n")
    elif your_guess < correct_number:
        print("You have chosen a smaller number. Guess a bigger one please ... \n")
    
    if iterations == 10 or iterations == 20 or iterations == 30:
        quit_or_not = int(input("You have made 10 attempts. Would you like to tell you the number and quit? [1->Yes | 0->No] : "))
        if quit_or_not == 1:
            print("The magic number is: 28. Now exiting ... Bye bye ... \n")
            break
        else:
            print("Continue guessing the nmagic number ... \n")

    iterations += 1
