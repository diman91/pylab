#
# For loops in Python
#

numbers = [1, 2, 3, 4, 5]
for i in numbers:
    print(i)

print("")

for i in range(5):
    print(i)


print("")

for i in range(1, 6):
    print(i)

print("")

for i in range(0, 21, 4):
    print(i)



for even in range(1, 100):
    if even % 2 == 0:
        print(str(even) + " is even number ..")


for letter in "dimosthenis":
    print(letter.upper())
