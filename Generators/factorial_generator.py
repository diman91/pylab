
def generator(num):
    f = 1
    for i in range(1, num):
        yield f
        f *= i

def main():
    N = 11
    j = 0
    for i in generator(N):
        print(f"{j}! = {i}")
        j+=1

if __name__ == "__main__":
    main()
