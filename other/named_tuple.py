###################
## Named Tuples  ##
###################

from collections import namedtuple


def isEqual(c1, c2):
    if c1 == c2:
        print(f"Complex numbers < {c1} > & < {c2} > are equal.\n")
    else:
        print(f"Complex numbers < {c1} > & < {c2} > are NOT equal.\n")

def main():
    Complex = namedtuple('Complex', ('real', 'imag'))
    c1 = Complex(3, 4)
    c2 = Complex(1, 2)

    real1 = c1[0]
    imag1 = c1[1]

    real2 = c2[0]
    imag2 = c2[1]
    
    for i in c1:
        print(i)

    c3 = c1+c2
    print(c3)

    c4 = Complex(4, 6)

    #
    # Check differences in contrast to the ../magicDefs/advUne.py file's Complex class
    # This is a namedtuple simple/trivial example anyways ...
    #
    isEqual(c1, c2)
    isEqual(c2, c2)
    isEqual(c1+c2, c3)
    isEqual(c1+c2, c4)

if __name__ == "__main__":
    main()

