import threading


class A:
    def __init__(self, num):
        self.num = num
    def fact(self):
        f=1
        for i in range(1, self.num+1):
            f *= i
        print("%s! = %s" % (self.num, f))


def threaded_def(num):
    a = A(num)
    th = threading.Thread(target = a.fact())
    th.setName("Dimos Thread")
    print("Current thread name: %s " %(th.getName()) )
    th.start()

def main():
    # thread to run a class's function
    for i in range(1, 11):
        a = A(i)
        thread1 = threading.Thread(target=a.fact())
        thread1.start()
    print("Current thread: %s " %(threading.currentThread()))
    print("Current thread Name: %s " %(thread1.getName()))
    # threaded function
    print("\nThreaded Function test : ")
    threaded_def(11)
    threaded_def(22)
    print("Total number of threads: %s" %(threading.activeCount()))
    print("List of threads: %s" %(threading.enumerate()))

if __name__ == "__main__":
    main()


"""
    SAMPLE OUTPUT :
        
    1! = 1
    2! = 2
    3! = 6
    4! = 24
    5! = 120
    6! = 720
    7! = 5040
    8! = 40320
    9! = 362880
    10! = 3628800
    Current thread: <_MainThread(MainThread, started 139816514557760)> 
    Current thread Name: Thread-10 
    
    Threaded Function test : 
    11! = 39916800
    Current thread name: Dimos Thread 
    22! = 1124000727777607680000
    Current thread name: Dimos Thread 
    Total number of threads: 2
    List of threads: [<_MainThread(MainThread, started 139816514557760)>]

"""