# Checking python's split() function

def split_the_string(my_string):
    """ Split the string of a linux path using the '/' character """
    try:
        string_list = my_string.split('/')
        print("The splitted path string is: ")
        for item in string_list:
            print(item)
    except Exception as e:
        print("An exception occured. Please try again!")
        print("Exception %s" % e)

def main():
    """ main definition/function """
    linux_path = '/home/giovanni/Documents/PyLab'
    if(not len(linux_path)):
        print("Empty string. Please try again!")
        exit(1)
    print("The path is: %s" % linux_path)
    print()
    split_the_string(linux_path)

if __name__ == "__main__":
    main()