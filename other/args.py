#
# Arguments & Positional arguments trivial example
#

def print_list(*args):
	for item in args:
		print(item)

def print_dict(**kwargs):
	#for item in kwargs.items():
	#	print(item)

	for key, val in kwargs.items():
		print(f"{key} : {val}")
		
	#for val in kwargs.values():
	#	print(val)


def main():
	my_list = ["Ronaldo", "Rivaldo", "Ronaldinho", "Gilberto"]
	my_dict = { 
		"Brazil": "Ronaldo",
		"France": "Mbappe",
		"Netherlands": "Van Basten",
		"England": "Rooney",
		"Argentina": "Messi"
	}
	
	my_tuple = ("Greece", "Turkey", "Bulgaria", "Romania", "Serbia")
	
	print("Printing the list using args ..\n")
	print_list(*my_list)
	
	print("\nPrinting the dictionary using positional args (kwargs) .. \n")
	print_dict(**my_dict)
	
	print("\nPrinting the tuple using args .. \n")
	print_list(*my_tuple)
	
	other = [*"Unpacked it is .."]
	print(other)
	
if __name__ == "__main__":
	main()
	

