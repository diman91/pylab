#
# The property() function in action using a Complex number class via Decorators
#

class Complex:

    def __init__(self, real, imag):
        self._real = real
        self._imag = imag

    @property
    def real(self):
        print("Get real")
        return self._real
    
    @property
    def imag(self):
        print("Get imag")
        return self._imag
    
    @real.setter
    def _set_real(self, val):
        print("Set real")
        self._real = val
    
    @imag.setter
    def _set_imag(self, val):
        print("Set imag")
        self._imag = val

    @real.deleter
    def real(self):
        print("Del real")
        del self._real

    @imag.deleter
    def imag(self):
        print("Del imag")
        del self._imag


    def __str__(self):
        return f"Y = {self._real} + {self._imag} * j\n"

    def __add__(self, other):
        return Complex(self.real + other.real, self.imag + other.imag)
