#
# The property() function in action using a Complex number class
#

class Complex:

    def __init__(self, real, imag):
        self._real = real
        self._imag = imag

    def _get_real(self):
        print("Get real")
        return self._real
    
    def _get_imag(self):
        print("Get imag")
        return self._imag
    
    def _set_real(self, val):
        print("Set real")
        self._real = val
    
    def _set_imag(self, val):
        print("Set imag")
        self._imag = val

    def _del_real(self):
        print("Del real")
        del self._real

    def _del_imag(self):
        print("Del imag")
        del self._imag
    
    real = property(
        fget = _get_real,
        fset = _set_real,
        fdel = _del_real,
        doc = "The real part of the Complex number"
    )

    imag = property(
        fget = _get_imag,
        fset = _set_imag,
        fdel = _del_imag,
        doc = "The imag part of the Complex number"
    )

    def __str__(self):
        return f"Y = {self._real} + {self._imag} * j\n"

    def __add__(self, other):
        return Complex(self.real + other.real, self.imag + other.imag)
