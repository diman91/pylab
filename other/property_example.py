#
# Property() function in Python classes example
#

from complex_simple import Complex

def main():
    c1 = Complex(1, 2)
    print()
    c2 = Complex(3, 2)
    print()
    c3 = c1 + c2
    print()

    print(c1)

    del c1.real
    c1._set_real(22)
    print(c1)

    print(c3)

if __name__ == "__main__":
    main()
    