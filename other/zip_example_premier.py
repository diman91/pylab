#
# zip() function example
#

def main():
    teams_nums = [1, 2, 3, 4, 5]
    teams_names = ["Olympiacos", "Panathinaikos", "PAOK", "AEK", "Aris"]
    zipped_teams = zip(teams_nums, teams_names)
    print("Type of zipped object is :" + str(type(zipped_teams)))
    print(f"Zipped object as a list is : {list(zipped_teams)}\n")

    teams_dict = dict()

    for team_id, team_name in zip(teams_nums, teams_names):
        print(f"Team #{team_id} ==> {team_name}")
        teams_dict[team_id] = team_name

    print(f"\nTeams created dictionary is : {teams_dict}\n\nTeams dictionary values are:\n")

    for item in teams_dict.values(): # teams_dict.items() || teams_dict.keys()
        print(item)

    ##
    ## Unzipping
    ##
    tms = [(1, "Olympiacos"), (2, "Panathinaikos"), (3, "PAOK"), (4, "AEK"), (5, "Aris")]
    ids, nms = zip(*tuple(tms))
    print("\nUnzipping list of tuples ::  ")
    print(ids)
    print(nms)

if __name__ == "__main__":
    main()

