#
# Property() function in Python classes example via Decorator
#

from complex_decorated import Complex

def main():
    c1 = Complex(1, 2)
    print()
    c2 = Complex(3, 2)
    print()
    c3 = c1 + c2
    print()

    print(c1)

    print(c3.imag)

    del c1.real
    del c1.imag

    print(c3)

if __name__ == "__main__":
    main()