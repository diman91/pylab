from enum import Enum

class Colors(Enum):
    '''
        Enumeration of Colors
    '''
    white = 1
    black = 2
    red = 3
    green = 4
    blue = 5

class Brands(Enum):
    '''
        Enumeration of Brands
    '''
    Pinarello = 1
    Eddy_Merckxx = 2
    BMC = 3
    Bianchi = 4
    Fuji = 5


def main():
    [print(color.name) for color in Colors]
    print()
    [print(brand) for brand in Brands]

    print()

    for i in range(1, len(Brands)+1):
        print(Brands(i).name)


if __name__ == "__main__":
    main()

''' Sample Output:

white
black
red
green
blue

Brands.Pinarello
Brands.Eddy_Merckxx
Brands.BMC
Brands.Bianchi
Brands.Fuji

Pinarello
Eddy_Merckxx
BMC
Bianchi
Fuji

'''
