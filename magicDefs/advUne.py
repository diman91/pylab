###
# Advanced features Part I: 
# print(class object) -- object1 == object2 -- object1 + object2
##

class Complex:
    '''
        Complex numbers class 
    '''

    def __init__(self, real, imag):
        '''
            CTor
        '''
        self.real = real
        self.imag = imag

    def __str__(self):
        '''
            Overload print() operator like in c++
        '''
        return f"Y = {str(self.real)} + {self.imag}*j "

    def __add__(self, rhs):
        '''
            overload + operator in python like in c++
        '''
        re = self.real + rhs.real
        im = self.imag + rhs.imag
        return Complex(re, im)
    
    def __eq__(self, other):
        '''
            In order to compare two Complex numbers
        '''
        if isinstance(other, Complex):
            return self.real == other.real and self.imag == other.imag
        return False

def isEqual(c1, c2):
    if c1 == c2:
        print(f"Complex numbers < {c1} > & < {c2} > are equal.\n")
    else:
        print(f"Complex numbers < {c1} > & < {c2} > are NOT equal.\n")

def main():
    c1 = Complex(3, 4)
    c2 = Complex(1, 2)
    print(c1)
    print(c2)
    c3 = c1 + c2
    print(c3)

    isEqual(c1, c2)
    isEqual(c1+c2, c3)

if __name__ == "__main__":
    main()

