
class Players:
    """
        A student class for dictionaries exploration in python
        dictionary = { key1: value1, key2: value2, ... }
    """
    dict = {1050189: "Giovanni Silva", 1050820: "Rivaldo Boba Ferreira",
            1050821: "Zlatko Zahovic", 1050822: "Karembue",
            1050823: "Par Zetterberg", 1050824: "Luciano Galletti"}
    def getSize(self):
        return len(self.dict.keys())
    def add(self, id, name):
        self.dict[id] = name
    def remove(self, id):
        del self.dict[id]
    def printPlayers(self):
        try:
            for key in self.dict:
                print(self.dict[key])
            print
        except Exception as e:
            print("The %s Exception occured. Please try again!" % e)

    def clearTheDict(self):
        # Delete last items from the dictionary using the .popitem() built-in function
        id1, player1 = self.dict.popitem()
        id2, player2 = self.dict.popitem()
        print("Just deleted %s, %s from the dictionary of players ... " % (player1, player2) )
        print("Dictionary length before clear: %d" % self.getSize())
        # Python's Dictionary built-in method
        self.dict.clear()
        print("Dictionary length after clear: %d" % self.getSize())

def main():
    one = Players()
    one.printPlayers()
    # add djole in thge list
    print("Adding Predrag Djordjevic ..\n")
    one.add(1050825, "Predrag Djordjevic")
    print("Adding Predrag Djordjevic 2nd time ..\n")
    one.add(1050825, "Predrag Djordjevic")
    one.printPlayers()
    # delete Karembue from the list
    one.remove(1050822)
    one.printPlayers()
    # Clear the dictionary
    one.clearTheDict()
    one.printPlayers()

if __name__ == "__main__":
    main()
