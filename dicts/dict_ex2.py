import os

class Info:
    """
        Dictionaries example #2
    """
    commands_dict = dict()

    def __init__(self):
        """
            Initialize dictionary with a few command key-values
        """
        self.commands_dict["useradd"] = "create a new user or update default new user information" # os.popen("whatis useradd").read()
        self.commands_dict["userdel"] = "delete a user account and related files"
        self.commands_dict["usermod"] = "modify a user account"
        self.commands_dict["groupadd"] = "create a new group"
        self.commands_dict["groupdel"] = "delete a group"
        self.commands_dict["groupmod"] = "modify a group definition on the system"
        self.commands_dict["ip6tables"] = "administration tool for IPv4/IPv6 packet filtering and NAT"

    def rmv(self, command):
        """
            Remove a command entry from the dictionary
        """
        # Same as :: << if command not in self.commands_dict: >> 
        #
        if command not in self.commands_dict.keys():
            print("**** Sorry cannot remove non-existent element from the dictionary! Try again using another key! ****\n")
        else:
            del self.commands_dict[command]

    def __str__(self):
        """
            Overload print def to work in another way instead of default one
        """
        print "\t\t\t*** COMMANDS HELP START *** \n"
        if len(self.commands_dict) == 0:
            print("Sorry, I cannot print exmpty dictionary ...\n")
        else:
            for key in self.commands_dict:
                print("%s \t -  %s\n" %(key, self.commands_dict[key]))
        
        return "\n\t\t\t*** COMMANDS HELP END *** \n"
        

def main():
    """
        main def
    """
    obj = Info()
    print(obj)
    obj.rmv("groupmod")
    obj.rmv("groupadd")
    obj.rmv("groupdel")
    obj.rmv("doesnotexist")
    print(obj)


if __name__ == "__main__":
    main()