# dict example 4
# dict built-in iteration methods
#

def main():
  my_dict = {
    "Developer": "Kostas",
    "Engineer": "Nikos",
    "Operations": "Dimitris",
    "Infrastructure": "Viktor",
    "IP Networking": "Andrea"
  }
  print('----------------------------------------------------')

  for key in my_dict:
      print(key + "\t-->\t" + my_dict[key])

  print('----------------------------------------------------')

  for item in my_dict.items():
      print(item)

  print('----------------------------------------------------')

  for key, value in my_dict.items():
      print(key + "\t-->\t" + value)

  print('----------------------------------------------------')

  for key in my_dict.keys():
      print(key + "\t-->\t" + my_dict[key])

  print('----------------------------------------------------')

  for value in my_dict.values():
      print(value)

  print('----------------------------------------------------')

if __name__ == "__main__":
    main()
