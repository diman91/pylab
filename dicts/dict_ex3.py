import math

class Complex:
    def __init__(self, real = 0.0, image = 0.0):
        self.real = real
        self.image = image
        self.ComplexDict = dict()   # or ... = {}
    
    def __del__(self):
        self.real = 0.0
        self.image = 0.0

    def getReal(self):
        return self.real
    def getImage(self):
        return self.image
    
    def setReal(self, re):
        self.real = re
    def setImage(self, img):
        self.image = img

    def Norm(self):
        """
            |z| = |Real| + |Imaginary| = |self.real| + |self.image| = sqrt(self.real*self.real + self.image*self.image)
        """
        return math.sqrt( math.pow(self.real, 2) + math.pow(self.image, 2) )

    def conj(self):
        return Complex(self.real, -self.image)
    
    def __add__(self, rhs):
        """
            z = (z1 + z2)
        """
        newReal = self.real + rhs.real
        newImage = self.image + rhs.image
        return Complex(newReal, newImage)

    def __sub__(self, rhs):
        """
            z = (z1 - z2)
        """
        return Complex(self.real - rhs.real, self.image - rhs.image)

    def __str__(self):
        """
            print def for a complex number
        """
        if self.image > 0:
            print("Complex number : z = %s + j*%s\n" %(self.getReal(), self.getImage()))
        elif self.image < 0:
            print("Complex number : z = %s - j*%s\n" %(self.getReal(),  abs(self.getImage()) ))
        elif self.real == 0 and self.image == 0:
            print("Complex number : z = 0\n")

        print("|z| = %s\n" %str(self.Norm()))
        return ""

    def set_Cplx_dict(self):
        """
            dictionary with complex numbers
        """
        self.ComplexDict["Complex   Number = "] = str(self.real) + " + j*" + str(self.image)
        self.ComplexDict["Conjugate Number = "] = str(self.real) + " - j*" + str(self.image)
        norm = "|" + str(self.real) + " + j*" + str(self.image) + "| = "
        self.ComplexDict[norm] = self.Norm()
        return self.ComplexDict

def conjugate(re, im):
    return Complex(re, -im)

def main():
    """
        main def
    """
    z1 = Complex(-3.3, 4.4)
    z2 = conjugate(z1.real, z1.image)
    # Same as :: z2 = z1.conj() 
    ## z3 = Complex(-3, 4)
    ## print(z1)
    ## print(z2)
    ## print(z1 + z2)
    ## print(z1 - z2)

    ## z5 = Complex(1,2)
    ## z6 = Complex(-1,-2)
    ## print(z5 + z6)

    z1_dict = z1.set_Cplx_dict()
    for key, value in z1_dict.items():
        print("%s %s\n" %(key, value))


if __name__=="__main__":
    main()

