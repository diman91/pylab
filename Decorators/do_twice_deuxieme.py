def do_twice(func):
    def wrapper_do_twice(*args, **kwargs):
        print("Inside wrapper ...\n")
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper_do_twice

@do_twice
def say_whee():
    print("Inside say_whee() ...\n")
    return "Whee!\n"

@do_twice
def say_hello(name):
    print("Inside say_hello() ...\n")
    return f"Hello {name}!\n"

une = say_whee()
print(une)

deux = say_hello("Dimos")
print(deux)


''' Sample Output ::
Inside wrapper ...

Inside say_whee() ...

Inside say_whee() ...

Whee!

Inside wrapper ...

Inside say_hello() ...

Inside say_hello() ...

Hello Dimos!
'''
