
def html_tag(tag_outer, tag_inner = None):
    def wrapper(func):
        def inner(*args, **kwargs):
            if tag_inner is None:
                return f"<{tag_outer}>\n\t{func(*args, **kwargs)}\n</{tag_outer}>\n"
            else:
                return f"<{tag_outer}>\n\t<{tag_inner}>\n\t\t{func(*args, **kwargs)}\n\t</{tag_inner}>\n</{tag_outer}>\n"
        return inner
    return wrapper

@html_tag("div", "p")
def div_p(inner):
    return inner

@html_tag("p4")
def p4(inner):
    return inner

print(p4("Dimos has a paragraph of 'p4' ..."))
print(div_p("This is a paragraph!"))


''' Sample Output ::
<p4>
        Dimos has a paragraph of 'p4' ...
</p4>

<div>
        <p>
                This is a paragraph!
        </p>
</div>
'''