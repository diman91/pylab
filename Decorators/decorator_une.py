##
## Inner function | Return a function from another function
##



def decorate_car(func):
    def paint(*args, **kwargs):

        print("Before painting the car ... \n")

        # getting the returned value
        ret_val = func(*args, **kwargs)
        print("After painting the car ... \n")

        # returning the value to the original frame
        return ret_val.upper()

    return paint


# adding decorator to the function
@decorate_car
def create_car(brand, model):
    return str(f"Creating {brand} {model} ... [ DONE ] \n")



def main():
    '''
        main
    '''
    car = ("Toyota", "Corolla")

    # getting the value through return of the function
    print(create_car(car[0], car[1]))

    ## If I remove @decorate_car from create_car
    ## then I have to call this way
    ##
    # func = decorate_car(create_car)
    # print(func(a, b))

if __name__ == "__main__":
    main()
