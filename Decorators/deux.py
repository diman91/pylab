##
## Inner function | Return a function from another function
##

def speak(firstName):
    def sayHello(lastName):
        return f"Hello, my name is {firstName} {lastName}. Nice 2 meet U my friend!"

    return sayHello


def main():
    f = speak("Dimosthenis")
    print(f("Katsimardos"))

if __name__ == "__main__":
    main()
