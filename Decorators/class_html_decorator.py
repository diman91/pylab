import functools


class HTML_Decorator(object):

    def __init__(self, tag_outer, tag_inner = None):
        self.tag_outer = tag_outer
        self.tag_inner = tag_inner

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            if self.tag_inner is None:
                return f"<{self.tag_outer}>\n\t{func(*args, **kwargs)}\n</{self.tag_outer}>\n"
            else:
                return f"<{self.tag_outer}>\n\t<{self.tag_inner}>\n\t\t{func(*args, **kwargs)}\n\t</{self.tag_inner}>\n</{self.tag_outer}>\n"
        return wrapper
  

@HTML_Decorator('p')
def p(inner):
    return inner


@HTML_Decorator('div', 'p')
def div_p(inner):
    return inner


print(p("This is just a paragraph!"))
print(div_p("This is a paragraph inside a div!"))
