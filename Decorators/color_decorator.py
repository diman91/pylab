class Colors:
    """ Colorize print-outs output
    """
    CRED = '\033[91m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CVIOLETBG = '\33[45m'
    CEND = '\033[0m'

def decorate(mode):
    def wrapper(func):
        def inner(*args, **kwargs):
            if mode == "info":
                return f"{Colors.CGREEN} [INFO]  {func(*args, **kwargs)} {Colors.CEND}"
            elif mode == "warn":
                return f"{Colors.CVIOLETBG} [WARN]  {func(*args, **kwargs)} {Colors.CEND}"
            elif mode == 'error':
                return f"{Colors.CRED} [ERROR]  {func(*args, **kwargs)} {Colors.CEND}"
            else:
                return f"{Colors.CYELLOW} [UNKNOWN]  {func(*args, **kwargs)} {Colors.CEND}"
        return inner
    return wrapper


@decorate("info")
def info(inner):
    return inner

@decorate("warn")
def warn(inner):
    return inner

@decorate("error")
def error(inner):
    return inner

@decorate("dimos")
def test(inner):
    return inner

print(info("Hello, this is an informational message."))

print(warn("This is a warning. Next time be very careful what you say to anybody!"))

print(error("You made a lot of mistakes. Next time be extra cautious!"))

print(test("This is just a test!"))

