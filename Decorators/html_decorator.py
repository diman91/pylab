
def html_tag(tag):
    def wrapper(func):
        def inner(*args, **kwargs):
            return f"<{tag}>\n\t{func(*args, **kwargs)}\n</{tag}>\n"
        return inner
    return wrapper


@html_tag('div')
def div(inner):
    return inner

@html_tag('p')
def p(inner):
    return inner

@html_tag('li')
def li(inner):
    return inner

@html_tag('ul')
def ul(inner):
    return inner    

@html_tag('div')
@html_tag('p')
def div_p(inner):
    return inner


print(div("Python is cool!"))
print(p("Python is easy!"))
print(div_p('Python is powerfull!'))


my_list = ["Strings", "Functions", "Classes", "Decorators", "Dictionaries", "Generators"]
print('<ul>')
for item in my_list:
    print(li(item))
print('</ul>')

