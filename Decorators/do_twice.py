
def do_twice(func):
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        func(*args, **kwargs)
    return wrapper_do_twice

@do_twice
def say_whee():
    print("Whee!\n")

@do_twice
def say_hello(name):
    print(f"Hello {name}!\n")

say_whee()
say_hello("Dimos")


