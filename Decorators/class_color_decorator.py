class Colors:
    """ 
        Colorize print-outs output
    """
    CRED = '\033[91m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CVIOLETBG = '\33[38m'
    CEND = '\033[0m'

class ColorDecorate:

    def __init__(self, mode):
        ''' 
            CTor
        '''
        self.mode = mode

    def __call__(self, func):
        
        def wrapper(*args, **kwargs):
            if self.mode == "info":
                return f"{Colors.CGREEN} [INFO]  {func(*args, **kwargs)} {Colors.CEND}"
            elif self.mode == "warn":
                return f"{Colors.CVIOLETBG} [WARN]  {func(*args, **kwargs)} {Colors.CEND}"
            elif self.mode == 'error':
                return f"{Colors.CRED} [ERROR]  {func(*args, **kwargs)} {Colors.CEND}"
            else:
                return f"{Colors.CYELLOW} [UNKNOWN]  {func(*args, **kwargs)} {Colors.CEND}"
        
        return wrapper

@ColorDecorate("info")
def info(inner):
    return inner

@ColorDecorate("warn")
def warn(inner):
    return inner

@ColorDecorate("error")
def error(inner):
    return inner

@ColorDecorate("dimos")
def test(inner):
    return inner

print(info("Hello, this is an informational message."))

print(warn("This is a warning. Next time be very careful what you say to anybody!"))

print(error("You made a lot of mistakes. Next time be extra cautious!"))

print(test("This is just a test!"))

