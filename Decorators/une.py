##
## Starting from scratch | Build-up knowledge | Check functions call with function as arg
##

def sayHello(name):
    return f"Hello {name}!".upper()

def whisper(name):
    return str(sayHello(name)[0] + sayHello(name).lower()[1:])


def greet(function):
    f = function("diman")
    print(f)

def main():
    name = str("dimos")
    print(sayHello(name))
    print(whisper(name))

    greet(sayHello)
    greet(whisper)


if __name__ == "__main__":
    main()
