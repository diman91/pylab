import functools

def do_twice(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper

@do_twice
def say_hello(name = None):
    if name is None:
        print("Hello there!\n")
    else:
        print(f"Hello {name}!\n")

say_hello()
say_hello("dimos")

print(say_hello.__name__)
