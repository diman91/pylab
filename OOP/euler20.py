class DigitSum:
    def fact(self, num):
        """Calc the factorial of a num"""
        if(num==0 or num==1):
            return 1
        return num*self.fact(num-1)
    def get_digits_sum(self, num):
        """
        Calc the digits sumation of a factorial num
        10! = 3628800 = 3+6+2+8+8+0+0 = 27
        """
        sum=0
        while(num!=0):
            sum += num%10
            num = num/10
        return sum

if __name__ == "__main__":
    diman = DigitSum()
    for num in range(100, 101):
        fact = diman.fact(num)
        print("%s! = %s" %(num, fact))
        print("Digits sumation(%s!) = %s" %(num, diman.get_digits_sum(fact)))
