import sys

class Recursion:
    """ factorial and fibonacci using recursion for demostration """
    def fact(self, num):
        """ 5! = 5*4*3*2*1 = 120 """
        if(num == 0 or num == 1):
            return num
        return num * self.fact(num-1)
    def fibonacci(self, how_many):
        """ 1 2 3 5 8 13 ... = 1 + (num + previous_num) + the same goes on ... """
        if(how_many <= 1):
            return 1
        return self.fibonacci(how_many-1) + self.fibonacci(how_many-2)

    def rec_sum(self, num):
        if(num == 0 or num == 1):
            return num
        return num + self.rec_sum(num-1)

def main():
    test1 = Recursion()
    print("10! = %s" % test1.fact(10))
    # Print 12 Fibonacci numbers
    print("Printing the 12 first fibo nums: ")
    for i in range(1,13):
        sys.stdout.write(str(test1.fibonacci(i)) + " ")
    print("")
    for i in range(0, 10):
        print("Sum(0..%s) = %s" %(i, test1.rec_sum(i)))


if __name__ == "__main__":
    main()

"""
OUTPUT: 
    10! = 3628800
    Printing the 12 first fibo nums: 
    1 2 3 5 8 13 21 34 55 89 144 233 
"""