
class NegativeNumberException(Exception):
    pass

class EmptyStringException(Exception):
    pass


class Human(object):
    def __init__(self, firstName, lastName, age):
        self._firstName = firstName.lower()
        self._lastName = lastName.lower()
        self._age = age

    @property
    def firstName(self):
        return self._firstName

    @property
    def lastName(self):
        return self._lastName

    @property
    def age(self):
        return self._age

    @firstName.setter
    def firstName(self, fn):
        if len(fn) == 0:
            # raise ValueError("First Name cannot be empty!")
            raise EmptyStringException
        else:
            self._firstName = fn

    @lastName.setter
    def lastName(self, ln):
        if len(ln) == 0:
            # raise ValueError("Last Name cannot be empty!")
            raise EmptyStringException
        else:
            self._lastName = ln

    @age.setter
    def age(self, value):
        if value > 0:
            self._age = value
        else:
            raise NegativeNumberException

    @property
    def sayHello(self):
        print("Hello, my name is %s %s and I am %d y.o.\n" % (self._firstName, self._lastName, self._age))


class Person(Human):
    @property
    def sayHello(self):
        print("Species -> homo sapiens")
        # Call the parent class sayHello() def
        super(Person, self).sayHello

    @classmethod
    def messi(cls):
        return cls("Lionel", "Messi", 34)


def main():
    ribo = Person("riVALDo", "fERREIra", 50)
    ribo.sayHello
    gio = Human("GiovaNNI", "sILvA", 50)
    gio.sayHello

    gio.firstName = "DIMOS".lower()
    print(gio.firstName)

    messi = ribo.messi()
    messi.sayHello

    gio.age = 44
    print(gio.age)


if __name__ == "__main__":
    main()
