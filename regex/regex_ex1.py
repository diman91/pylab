# regexes | example 1
#
import re

def findDates(myString):
    # Create the regex pattern | 'd' stands for digit
    datePattern = '(\d{2}):(\d{2}):(\d{2})' 
    # matchedDate = re.search(datePattern, myString)
    # search for this regex inside the given string
    matchedDate = re.search(datePattern, myString)
    # 
    print("Hour: %s | Minutes: %s | Seconds: %s\n" %(matchedDate.group(1), matchedDate.group(2), matchedDate.group(3)))

    
def main():
    dimanString = "I was born on 12:12:21 , I am going to die on 21:12:12 ... Ciao \n"
    findDates(dimanString)

if __name__ == "__main__":
    main()
