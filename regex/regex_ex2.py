# regexes | example 1
#
import re

def findDates(myString):
    # Create the regex pattern | 'd' stands for digit
    dateRegex = re.compile(r'(\d{2}):(\d{2}):(\d{2})')
    # Find all occurences now please | Enough with games
    allOccs = dateRegex.findall(myString)
    # Print them out please || One by one
    index = 1
    for item in allOccs:
        print("Date Occurence #%d: %s\n" %(index, item))
        index += 1
    #print(allOccs)
    
def main():
    dimanString = "I was born on 12:12:21 , I am going to die on 22:33:55 ... Ciao 55:88:99 \n"
    findDates(dimanString)

if __name__ == "__main__":
    main()


""" OUTPUT: 

Date Occurence #1: ('12', '12', '21')

Date Occurence #2: ('22', '33', '55')

Date Occurence #3: ('55', '88', '99')

"""