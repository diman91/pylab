import json

with open('info.json', 'r') as file:
    info = json.load(file)

print(type(info))
print(f"Info as is from json file directly: {info}")
print()

print("And info by extracting all key-values one by one: \n")
for k, v in info.items():
    print(f"{k}: {v}")
