import json

info = {
    'Name': 'Dimosthenis',
    'Address': 'Ladonos 34',
    'Town': 'Patras',
    'Country': 'Greece'
}

with open('info.json', 'w') as file:
    json.dump(info, file)
